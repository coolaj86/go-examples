# envpath

**WIP**

An example of getting, setting, and updating PATHs on \*nix (Linux, Mac, etc).

```bash
envpath append /opt/whatever/bin
```

```bash
New sessions will have /opt/whatever/bin in their PATH.
To update this session run

    export PATH="$PATH:/opt/whatever/bin"
```
