# Go Zip Example

An example of how to zip a directory in Go.

-   Utilizes `filepath.Walk` to traverse a directory (or single file)
-   Handles each of
    -   Files (deflated, compressed)
    -   Directories (empty, not compressed)
    -   Symlinks (not compressed)
    -   Skips irregular files (pipes, sockets, devices, chars)
-   Names zip file after the name of the directory
-   Trims path prefix

```bash
git clone https://git.coolaj86.com/coolaj86/go-examples.git
pushd go-examples/zip
```

```bash
go run . path/to/whatever
```

```txt
wrote whatever.zip
```

Separates concerns into functions for readability:

-   func main()
-   func Zip(w io.Writer, src string, trim string) error
    -   zipOne
        -   zipDirectory
        -   zipFile
        -   zipSymlink
